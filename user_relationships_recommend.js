/**
 * jQuery behaviors for User Relationships Recommend.
 */
Drupal.behaviors.user_relationships_recommend = function(context) {
  // Add click handlers to the "dismiss" links in our Views blocks
  // (actually, to the <span> tags surrounding the links).
  $('.ur-recommend-dismiss', context).click(function(e) {
    // Hide any existing popups.
    Drupal.user_relationships_ui.hidePopup();

    // Calculate the popup window position.
    // This is mostly just copied from User Relationships UI.
    var position = Drupal.settings.user_relationships_ui.position.position;
    var left = Drupal.settings.user_relationships_ui.position.left;
    var top = Drupal.settings.user_relationships_ui.position.top;
    if (position == "fixed") {
      if (left <= 1) {
        left = Math.round(($(window).width()*left) - ($("#user_relationships_popup_form").width()/2));
      }
      if (top <= 1) {
        top = Math.round(($(window).height()*top));
      }
    }
    else {
      left = (e.pageX ? e.pageX : e.clientX) + Number(left);
      if (left + $("#user_relationships_popup_form").width() > $(window).width()) {
        left = (e.pageX ? e.pageX : e.clientX) - $("#user_relationships_popup_form").width();
      }
      top = (e.pageY ? e.pageY : e.clientY) + Number(top);
    }

    // Prepare the popup window.
    $('#user_relationships_popup_form')
      .css({'top' : top + 'px', 'left' : left + 'px', 'position' : position})
      .html(Drupal.user_relationships_ui.loadingAnimation())
      .slideDown();

    // Grad the recommendation ID from the span's id attribute.
    var rid = this.id.match(/\d+/);
    // Trigger the AJAX "dismiss" callback for this recommendation.
    $.get(Drupal.settings.basePath + 'user_relationships_recommend/ajax/' + rid + '/dismiss', null, dismissForm);
    return false;
  });

  // Parse the AJAX response and present the dismiss form in the popup.
  var dismissForm = function(response) {
    var result = Drupal.parseJson(response);
    $('#user_relationships_popup_form').html(result.form);
    // Set the "No" link to just close the popup, rather than redirecting.
    $('#user_relationships_popup_form').find('a[href="' + Drupal.settings.basePath + 'user"]').click(function(e) {
      e.preventDefault();
      Drupal.user_relationships_ui.hidePopup();
    });
  }
}
