<?php
/**
 * @file
 * Rules integration for User Relationships Recommend.
 */

/**
 * Implements hook_rules_event_info().
 */
function user_relationships_recommend_rules_event_info() {
  $info = array();

  // Content recommendations.
  $info['user_relationships_recommend_node'] = array(
    'label' => t('A user recommends content'),
    'module' => 'UR Recommend',
    'arguments' => user_relationships_recommend_rules_event_arguments('node', t('The recommended content')),
  );

  // User recommendations.
  $info['user_relationships_recommend_user'] = array(
    'label' => t('A user recommends a user'),
    'module' => 'UR Recommend',
    'arguments' => user_relationships_recommend_rules_event_arguments('user', t('The recommended user')),
  );

  return $info;
}

/**
 * Provides Rules event arguments for recommendations.
 *
 * @param $type
 *   The type of entity that was recommended.
 * @param $label
 *   The label for the recommended entity argument.
 *
 * @return
 *   An array to be used as arguments for an event in hook_rules_event_info().
 */
function user_relationships_recommend_rules_event_arguments($type, $label) {
  $args = array(
    'recommended' => array(
      'type' => $type,
      'label' => $label,
    ),
    'sender' => array(
      'type' => 'user',
      'label' => t('The user that sent the recommendation'),
    ),
    'recipient' => array(
      'type' => 'user',
      'label' => t('The user that received the recommendation'),
    ),
  );

  // Include the relationship, if the data type is supported.
  if ($type == 'user' && module_exists('user_relationships_rules')) {
    $args['relationship'] = array(
      'type' => 'relationship',
      'label' => t('The relationship that was recommended'),
    );
  }

  return $args + rules_events_global_user_argument();
}
