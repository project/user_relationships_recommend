<?php
/**
 * Views integration for User Relationships Recommend.
 */

/**
 * Implements hook_views_data().
 */
function user_relationships_recommend_views_data() {
  $data = array();

  // The recommendations table.
  $data['user_relationships_recommend']['table']['group'] = t('Recommendations');
  // Relationship to a node as the recommended entity.
  $data['user_relationships_recommend']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'entity_id',
    'extra' => array(array(
      'field' => 'entity_type',
      'value' => 'node',
    )),
  );
  // Relationship to a user as the recommended entity.
  $data['user_relationships_recommend']['table']['join']['users'] = array(
    'left_field' => 'uid',
    'field' => 'entity_id',
    'extra' => array(array(
      'field' => 'entity_type',
      'value' => 'user',
    )),
  );
  // Field; recommendation ID.
  $data['user_relationships_recommend']['rid'] = array(
    'title' => t('Rid'),
    'help' => t('The primary identifier for a recommendation.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'click sortable' => TRUE,
      'numeric' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  // Field; recommended by.
  $data['user_relationships_recommend']['sender_uid'] = array(
    'title' => t('Recommended by'),
    'help' => t('The user that sent the recommendation.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'label' => t('Recommended by'),
    ),
  );
  // Field; recommended to.
  $data['user_relationships_recommend']['recipient_uid'] = array(
    'title' => t('Recommended to'),
    'help' => t('The user that received the recommendation.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'label' => t('Recommended to'),
    ),
  );
  // Field; relationship type between sender and recipient.
  $data['user_relationships_recommend']['sender_recipient_rtid'] = array(
    'title' => t('Sender-recipient relationships'),
    'help' => t('The relationships that exist between the sender and the recipient.'),
    'real field' => 'sender_uid',
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'relationship field' => 'sender_uid',
      'base' => 'user_relationships',
      'base field' => 'requester_id',
      'label' => t('Relationships between sender and recipient'),
      'join_handler' => 'user_relationships_recommend_join_extra_field',
      'extra' => array(array(
        'left_field' => 'recipient_uid',
        'field' => 'requestee_id',
      )),
    ),
  );
  // Field; note.
  $data['user_relationships_recommend']['note'] = array(
    'title' => t('Note'),
    'help' => t('A note explaining why this recommendation was made.'),
    'field' => array(
      'handler' => 'views_handler_field_xss',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  // Field; dismiss link.
  $data['user_relationships_recommend']['dismiss_recommendation'] = array(
    'field' => array(
      'title' => t('Dismiss link'),
      'help' => t('Provide a simple link to dismiss the recommendation.'),
      'handler' => 'user_relationships_recommend_handler_field_link_dismiss',
    ),
  );

  // The user recommendations data table.
  $data['user_relationships_recommend_user']['table']['group'] = t('Recommendations');
  // Relationship to a user as the recommended entity, via the recommendation.
  $data['user_relationships_recommend_user']['table']['join']['users'] = array(
    'left_table' => 'user_relationships_recommend',
    'left_field' => 'rid',
    'field' => 'rid',
  );
  // Field; relationship type.
  $data['user_relationships_recommend_user']['rtid'] = array(
    'title' => t('Recommended relationship'),
    'help' => t('The type of relationship that was recommended between two users.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'user_relationship_types',
      'base field' => 'rtid',
      'label' => t('Recommended relationship'),
    ),
  );

  return $data;
}

/**
 * Views join handler for joining on additional fields.
 *
 * This class changes the 'extra' parameter of the join definition to be an
 * array of items with the following keys:
 * - field: the field to join on (right field)
 * - left_field: the field to join to
 * - operator: defaults to =
 */
class user_relationships_recommend_join_extra_field extends views_join {
  function join($table, &$query) {
    if (empty($this->definition['table formula'])) {
      $right_table = "{" . $this->table . "}";
    }
    else {
      $right_table = $this->definition['table formula'];
    }

    if ($this->left_table) {
      $left = $query->get_table_info($this->left_table);
      $left_field = "$left[alias].$this->left_field";
    }
    else {
      // This can be used if left_field is a formula or something. It should be
      // used only *very* rarely.
      $left_field = $this->left_field;
    }

    $output = " $this->type JOIN $right_table $table[alias] ON $left_field = $table[alias].$this->field";
    // Tack on the extra.
    if (isset($this->extra)) {
      if (is_array($this->extra)) {
        $extras = array();
        foreach ($this->extra as $info) {
          $operator = !empty($info['operator']) ? $info['operator'] : '=';

          // Join on the extra field.
          if ($this->left_table) {
            $extra_left_field = "$left[alias].$info[left_field]";
          }
          else {
            // This can be used if left_field is a formula or something. It
            // should be used only *very* rarely.
            $extra_left_field = $info[left_field];
          }
          $extras[] = "$extra_left_field $operator $table[alias].$info[field]";
        }

        if ($extras) {
          if (count($extras) == 1) {
            $output .= ' AND ' . array_shift($extras);
          }
          else {
            $output .= ' AND (' . implode(' ' . $this->extra_type . ' ', $extras) . ')';
          }
        }
      }
      else if ($this->extra && is_string($this->extra)) {
        $output .= " AND ($this->extra)";
      }
    }
    return $output;
  }
}

/**
 * Implements hook_views_handlers().
 */
function user_relationships_recommend_views_handlers() {
  return array(
    'handlers' => array(
      // Link field handlers.
      'user_relationships_recommend_handler_field_link_dismiss' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}
