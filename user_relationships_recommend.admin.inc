<?php
/**
 * @file
 * Configuration form for User Relationships Recommend.
 */

/**
 * Menu callback; the admin form.
 */
function user_relationships_recommend_admin() {
  $form = array();
  module_load_include('inc', 'user_relationships_api', 'user_relationships_api.api');

  // Get a list of all relationship types available as form options.
  $types = user_relationships_types_load();
  $options = array();
  foreach ($types as $type) {
    $options[$type->rtid] = $type->plural_name;
  }

  // Choose which relationships are allowed for sending recommendations.
  $form['user_relationships_recommend_allowed_to'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Relationships allowed to send recommendations'),
    '#default_value' => variable_get('user_relationships_recommend_allowed_to', array()),
    '#options' => $options,
    '#description' => t('Users may only send recommendations to other users with whom they have one of the relationships selected above.  If no relationship types are selected, users will not be able to send recommendations.'),
  );

  // Choose which relationship types a user can recommend for other users.
  $form['user_relationships_recommend_allowed_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Users can recommend that other users become'),
    '#default_value' => variable_get('user_relationships_recommend_allowed_types', array()),
    '#options' => $options,
    '#description' => t('For recommending one user to another, you may optionally limit the types of relationships that can be suggested.  For example, you may wish to allow recommendations that two other users become "friends", but not allow recommendations that they become "family."  If no relationship types are selected, all available types will be allowed.'),
  );

  return system_settings_form($form);
}
