<?php
/**
 * @file
 * Views field handler for the "dismiss" link.
 */

class user_relationships_recommend_handler_field_link_dismiss extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['rid'] = 'rid';
    $this->additional_fields['recipient_uid'] = 'recipient_uid';
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['text'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    // Ensure user has access to dismiss this recommendation.
    $recommendation = new stdClass();
    $recommendation->rid = $values->{$this->aliases['rid']};
    $recommendation->recipient_uid = $values->{$this->aliases['recipient_uid']};
    if (!user_relationships_recommend_access_recommendation($recommendation)) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('dismiss');
    return l($text, 'user_relationships_recommend/dismiss/' . $recommendation->rid, array('query' => drupal_get_destination()));
  }
}
