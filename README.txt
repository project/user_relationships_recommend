User Relationships Recommend
----------------------------
User Relationships Recommend allows users to send personal recommendations to
other users to whom they are related (via the User Relationships module).  Users
can recommend content that they believe others will find interesting, and can
recommend users that they believe others will want to add to their own
relationships.

This module depends on Views to output recommendations.  Default views have been
provided for listing recommendations received by the current user.  Rules events
have also been provided for triggering actions when recommendations are sent.

Requirements
------------
- User Relationships API
- Views: can be disabled, but needed if you want users to see recommendations
  that others have sent to them; no other output method has been provided.

Installation
------------
1. Copy the user_relationships_recommend folder to the appropriate Drupal
  directory (sites/all/modules or sites/default/modules).

2. Go to Administer >> Site building >> Modules and enable UR-Recommend.

3. Go to Administer >> Site configuration >> User Relationships Recommend and
  configure the module.  You must select at least one relationship type under
  "Relationships allowed to send recommendations" in order for users to begin
  making recommendations.

4. Go to Administer >> User management >> Permissions and grant permissions
  under user_relationships_recommend module.

  In order to recommend content, a user must have:
  - "recommend content" permission
  - "access content" permission (or, if node_access is in use, "view" access)

  In order to recommend users, a user must have:
  - "recommend users" permission
  - "access user profiles" permission
  - at least one relationship of a type allowed above in step 3

To send a recommendation
------------------------
Simply browse to a node or user and click the "recommend this" link, then
complete and submit the recommendation form as instructed.

Credits
-------
Written by Dan Lobelle (muriqui), muriqui.net.
Built for Mustardseed Media, Inc., www.mustardseedmedia.com.
