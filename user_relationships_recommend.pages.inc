<?php
/**
 * @file
 * User interface for User Relationships Recommend.
 */

/**
 * Base form builder for recommendation forms.
 *
 * @param $form_state
 *   The form's current state.
 * @param $exclude
 *   An array of user IDs to exclude from the recipients list.
 *
 * @return
 *   A Forms API array.
 */
function user_relationships_recommend_form(&$form_state, $exclude = array()) {
  $form = array();

  // Select recipients.
  global $user;
  $recipients = user_relationships_recommend_recipients($user, $exclude);
  $form['recommend'] = array();
  $form['recommend']['recipients'] = array(
    '#type' => 'select',
    '#title' => t('To'),
    '#options' => $recipients,
    '#multiple' => TRUE,
    '#required' => TRUE,
  );

  // Note field.
  $form['note'] = array(
    '#type' => 'textarea',
    '#title' => t('Note'),
  );

  // Action buttons.
  $form['actions'] = array(
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}

/**
 * Menu callback; the node recommendation form.
 */
function user_relationships_recommend_node_form(&$form_state, $node) {
  $form = user_relationships_recommend_form($form_state);
  $form['actions']['cancel'] = array(
    '#value' => l(t('Cancel'), 'node/' . $node->nid),
  );

  // Add the recommended node object for later reference.
  $form['recommend']['node'] = array(
    '#type' => 'value',
    '#value' => $node,
  );

  return $form;
}

/**
 * Submit function for the node recommendation form.
 */
function user_relationships_recommend_node_form_submit($form, &$form_state) {
  global $user;
  $node = $form_state['values']['node'];

  user_relationships_recommend_save($user->uid, $form_state['values']['recipients'], $node->nid, 'node', $form_state['values']['note']);
  drupal_set_message(t('Your recommendation has been sent.'));
  $form_state['redirect'] = 'node/' . $node->nid;
}

/**
 * Menu callback; the user recommendation form.
 */
function user_relationships_recommend_user_form(&$form_state, $account) {
  $form = user_relationships_recommend_form($form_state, array($account->uid));
  $form['actions']['cancel'] = array(
    '#value' => l(t('Cancel'), 'user/' . $account->uid),
  );

  // Change the recipients element to inline so we can render it as a sentence.
  $form['recommend']['#prefix'] = '<div class="container-inline">';
  $form['recommend']['#suffix'] = '</div>';
  $form['recommend']['recipients']['#multiple'] = FALSE;
  unset($form['recommend']['recipients']['#title']);
  $form['recommend']['#pre_render'] = array('_user_relationships_recommend_user_form_pre');

  // Add selection of relationship type.
  $allowed = variable_get('user_relationships_recommend_allowed_types', array());
  module_load_include('inc', 'user_relationships_api', 'user_relationships_api.api');
  $types = user_relationships_types_load();
  $options = array();
  foreach ($types as $type) {
    if (empty($allowed) || in_array($type->rtid, $allowed)) {
      $options[$type->rtid] = check_plain($type->name);
    }
  }
  $form['recommend']['relationships'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#required' => TRUE,
  );

  // Add the recommended user object for later reference.
  $form['recommend']['user'] = array(
    '#type' => 'value',
    '#value' => $account,
  );

  // Wrap the recommend element in a fieldset so that we can add a description.
  $form['recommend'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#description' => t('Note that if you select a two-way relationship, this recommendation will be sent to both users.'),
    'wrapper' => $form['recommend'],
  );

  return $form;
}

/**
 * Pre-render function to format the user recommendation form as a sentence.
 */
function _user_relationships_recommend_user_form_pre($element) {
  $recipients_html = drupal_render($element['recipients']);
  $relationships_html = drupal_render($element['relationships']);
  $account = $element['user']['#value'];
  $element['#value'] = t('Recommend that !recipient become !user\'s !relationship', array('!recipient' => $recipients_html, '!user' => theme('username', $account), '!relationship' => $relationships_html));
  return $element;
}

/**
 * Submit function for the user recommendation form.
 */
function user_relationships_recommend_user_form_submit($form, &$form_state) {
  global $user;
  $recommended_user = $form_state['values']['user'];
  $recipient = $form_state['values']['recipients'];
  $data = array('rtid' => $form_state['values']['relationships']);

  // Get the relationship type.
  module_load_include('inc', 'user_relationships_api', 'user_relationships_api.api');
  $relationship = user_relationships_type_load($data['rtid']);

  // Skip recipients for whom the relationship already exists.
  // We do this silently to avoid disclosing the recipient's relationship status
  // to the user submitting the form.
  if (!user_relationships_load(array('requester_id' => $recipient, 'requestee_id' => $recommended_user->uid, 'rtid' => $data['rtid']), array('count' => TRUE))) {
    user_relationships_recommend_save($user->uid, $recipient, $recommended_user->uid, 'user', $form_state['values']['note'], $data);
    // Send a reciprocal recommendation if the relationship is two-way.
    if (!$relationship->is_oneway) {
      user_relationships_recommend_save($user->uid, $recommended_user->uid, $recipient, 'user', $form_state['values']['note'], $data);
    }
  }

  drupal_set_message(t('Your recommendation has been sent.'));
  $form_state['redirect'] = 'user/' . $recommended_user->uid;
}

/**
 * Menu callback; the dismiss form.
 */
function user_relationships_recommend_dismiss_form(&$form_state, $recommendation, $ajax = FALSE) {
  $form['recommendation'] = array('#type' => 'value', '#value' => $recommendation);

  $sender = user_load($recommendation->sender_uid);
  switch ($recommendation->entity_type) {
    case 'user':
      $entity = user_load($recommendation->entity_id);
      $name = theme('username', $entity);
      break;
    case 'node':
      $entity = node_load($recommendation->entity_id);
      $name = l(check_plain($entity->title), 'node/' . $entity->nid);
      break;
  }
  $question = (empty($sender) || empty($entity)) ? t('Are you sure you want to dismiss this recommendation?') : t('Are you sure you want to dismiss this recommendation from !sender for !entity?', array('!sender' => theme('username', $sender), '!entity' => $name));

  if ($ajax) {
    // Use the question as the description when shown as a popup.
    return confirm_form($form, t('Dismiss recommendation'), 'user', $question, t('Yes'), t('No'));
  }
  return confirm_form($form, $question, 'user', NULL, t('Yes'), t('No'));
}

/**
 * Submit function for the dismiss form.
 */
function user_relationships_recommend_dismiss_form_submit($form, &$form_state) {
  $recommendation = $form_state['values']['recommendation'];
  user_relationships_recommend_delete($recommendation->rid);
  $form_state['redirect'] = 'user';
}
